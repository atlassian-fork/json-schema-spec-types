import * as Ajv from 'ajv';
import {JsonSchema} from '../../lib/json-schema';

// tslint:disable-next-line:no-var-requires
const jsonSchemaDraft7Schema = require('../../lib/json-schema-draft-07-schema.json');

interface ValidationResult {
    isValid: boolean;
    errors?: string;
}

const validateSchema = async (schema: JsonSchema): Promise<ValidationResult> => {
    const ajv = new Ajv();
    const isValid = await ajv.validate(jsonSchemaDraft7Schema, schema);
    const errors = ajv.errorsText();

    return {
        errors,
        isValid
    };
};

describe('json-schema-spec-types', () => {
    it('should validate a boolean schema', async () => {
        const booleanSchema: JsonSchema = true;

        const validationResult = await validateSchema(booleanSchema);

        expect(validationResult.isValid).toBe(true, `Schema not valid according to spec: ${validationResult.errors}`);
    });

    it('should validate a schema with all properties', async () => {
        // tslint:disable:object-literal-sort-keys
        const allPropertiesSchema: JsonSchema = {
            $schema: 'http://json-schema.org/draft-07/schema#',
            $id: '$id',
            $ref: '$ref',
            $comment: '$comment',
            title: 'title',
            description: 'description',
            default: true,
            readOnly: true,
            examples: [true, false],
            multipleOf: 1,
            maximum: 1,
            exclusiveMaximum: 1,
            minimum: 0,
            exclusiveMinimum: 0,
            maxLength: 1,
            minLength: 0,
            pattern: 'regex pattern',
            additionalItems: false,
            items: false,
            maxItems: 1,
            minItems: 0,
            uniqueItems: true,
            contains: false,
            maxProperties: 1,
            minProperties: 0,
            required: [],
            additionalProperties: true,
            definitions: {
                definition: true
            },
            properties: {
                property: true
            },
            patternProperties: {
                patternProperty: true
            },
            dependencies: {
                dependency: true
            },
            propertyNames: true,
            const: false,
            enum: [true, false],
            type: ['array', 'boolean', 'integer', 'null', 'number', 'object', 'string'],
            format: 'format',
            contentMediaType: 'contentMediaType',
            contentEncoding: 'contentEncoding',
            if: true,
            then: true,
            else: false,
            allOf: [true],
            anyOf: [true],
            oneOf: [true],
            not: false,
            xProperty: true
        };

        const validationResult = await validateSchema(allPropertiesSchema);

        expect(validationResult.isValid).toBe(true, `Schema not valid according to spec: ${validationResult.errors}`);
    });
});
