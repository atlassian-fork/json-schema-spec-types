'use strict';

const clean = require('gulp-clean');
const conventionalChangelog = require('gulp-conventional-changelog');
const exec = require('child_process').exec;
const filter = require('gulp-filter');
const fs = require('fs');
const git = require('gulp-git');
const gulp = require('gulp');
const jasmine = require('gulp-jasmine');
const minimist = require('minimist');
const ts = require('gulp-typescript');
const tslint = require('gulp-tslint');
const runSequence = require('run-sequence');

const options = minimist(process.argv.slice(2), {strings: ['type']});

const getBumpType = () => {
    const validTypes = ['major', 'minor', 'patch', 'prerelease'];
    if (validTypes.indexOf(options.type) === -1) {
        throw new Error(
            `You must specify a release type as one of (${validTypes.join(', ')}), e.g. "--type minor"`
        );
    }
    return options.type;
};

const getPackageJsonVersion = () => {
    return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
};

const tsProjectBuildOutput = ts.createProject('tsconfig.json', {noEmit: false});

const unitTests = 'build-output/test/unit/**/*.spec.js';

gulp.task('bump-version', (callback) => {
    exec(`npm version ${getBumpType()} --no-git-tag-version`, (err, stdout, stderr) => {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
});

gulp.task('changelog', () => {
    return gulp.src('CHANGELOG.md', {buffer: false})
        .pipe(conventionalChangelog({preset: 'angular'}))
        .pipe(gulp.dest('./'));
});

gulp.task('clean-build-output', () =>
    gulp.src('build-output', {force: true, read: false}).pipe(clean())
);

gulp.task('clean-copy-and-compile-build-output', (callback) => {
    runSequence(
        'clean-build-output',
        'compile-build-output',
        'copy-build-output-files',
        callback
    )
});

gulp.task('commit-changes', () => {
    return gulp.src('.')
        .pipe(git.add())
        .pipe(git.commit(`chore: release ${getPackageJsonVersion()}`));
});

gulp.task('compile-and-unit-test', () => {
    return compileBuildOutput()
        .pipe(filter([unitTests]))
        .pipe(jasmine({includeStackTrace: true}));
});

const compileBuildOutput = () => {
    const tsResult = tsProjectBuildOutput.src().pipe(tsProjectBuildOutput());
    return tsResult.js.pipe(gulp.dest('build-output'));
};

gulp.task('compile-build-output', () => {
    return compileBuildOutput();
});

gulp.task('copy-build-output-files', () => {
    return gulp.src('lib/json-schema-draft-07-schema.json').pipe(gulp.dest('build-output/lib'))
});

gulp.task('create-new-tag', (callback) => {
    const version = getPackageJsonVersion();
    git.tag(version, `Created Tag for version: ${version}`, callback);
});

gulp.task('default', (callback) => {
    runSequence(
        ['clean-copy-and-compile-build-output', 'lint-commits'],
        'lint-typescript',
        'test',
        callback
    );
});

gulp.task('lint-commits', (callback) => {
    exec('./node_modules/.bin/conventional-changelog-lint --from=06a1dc1 --preset angular',
        (err, stdout, stderr) => {
            console.log(stdout);
            console.log(stderr);
            callback(err);
        });
});

gulp.task('lint-typescript', () => {
    return tsProjectBuildOutput.src()
        .pipe(tslint())
        .pipe(tslint.report());
});

gulp.task('npm-publish', () => exec('npm publish'));

gulp.task('push-changes', (callback) => {
    git.push('origin', 'master', {args: '--tags'}, callback);
});

gulp.task('release', (callback) => {
    runSequence(
        'default',
        'bump-version',
        'changelog',
        'commit-changes',
        'create-new-tag',
        'push-changes',
        'npm-publish',
        callback
    );
});

gulp.task('test', () => {
    return gulp.src([unitTests]).pipe(jasmine({includeStackTrace: true}))
});

gulp.task('watch', ['clean-copy-and-compile-build-output'], () => {
    gulp.watch(['lib/**/*.ts', 'test/**/*.ts'], ['compile-and-unit-test'])
});
